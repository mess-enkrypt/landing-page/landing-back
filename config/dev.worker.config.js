module.exports = {
    apps : [{
	name        : "worker",
	script      : "src/worker/index.js",
	watch       : true,
	ignore_watch : ["node_modules", "data", ".git", "Dockerfile", "DockerfileCI", "docker-compose.yml", ".env", ".env.dist", "config/*", "#*#", "*~", "test/*", "config"],
	watch_options: {
	    "followSymlinks": false
	},
	exec_mode: "cluster"
    }]
}



FROM node:12-alpine
WORKDIR /usr/src/app

RUN apk update

#Install PM2 Global
RUN npm install -g pm2

# Copy package.json
COPY ./package.* .

# Install dep
RUN npm install

CMD ["pm2-runtime", "config/dev.config.js", "--watch"]

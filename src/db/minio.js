const aws = require('aws-sdk')
const multer = require('multer')
const multerS3 = require('multer-s3')

aws.config = new aws.Config({
    accessKeyId: <my access id>,
    secretAccessKey: <my secret key>,
    region: <my region>
})
const s3 = new aws.S3()
const upload = multer({
    storage: multerS3({
    s3: s3,
    acl: 'public-read',
    bucket: 'vue-express-upload',
    contentType: function(req, file,cb){
        cb(null, file.mimetype)
    },
    key: function (req, file, cb) {
        cb(null, file.originalname)
    }
    })
})

module.exports = upload

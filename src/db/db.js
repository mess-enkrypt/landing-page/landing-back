var mongoose = require('mongoose');
require('mongoose-type-email');

const mongo = process.env.MONGO_URL || "mongo";
const mongo_db_name = process.env.MONGO_DB_NAME || "mess-enkrypt";

mongoose.connect('mongodb://' + mongo + '/' + mongo_db_name, {useNewUrlParser: true});

mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open', function() {
    console.log("[DB] MongoDB connected");
});

module.exports = mongoose;

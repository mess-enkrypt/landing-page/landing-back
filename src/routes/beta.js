// Import Node Modules
const { check, validationResult } = require('express-validator');
var cors = require('cors');
const express = require('express');
const Queue = require('bee-queue');

// Import Misc Actions
const Timer = require('../actions/timer.js');
const log = require('../actions/discord.js');

const FeedbackModel = require('../models/Feedback.js');
const UserModel = require('../models/User.js');

const User = require('../actions/user.js');

var start = new Date();

// Import Random function from random.js                                                                                      
const Random = require("../actions/random.js");

const util = require('util')
				     
const cache = require('../db/cache.js').client;
const subscriber = require('../db/cache.js').subscriber;


const AddUserQueue = new Queue("user/add",   {
    redis: {
	host: 'redis'
    },
    isWorker: false
});

const DelUserQueue = new Queue("user/delete",   {
    redis: {
	host: 'redis'
    },
    isWorker: false
});

/* ROUTES */
const betaRegisterRoute = '/beta/register';
const betaFeedbackRoute = '/beta/feedback';
const betaDeleteRoute = '/beta/unregister';

module.exports = function(app){
    
    app.get('/download/:token', function(req, res){
	UserModel.find({"token": req.params.token}, function(error, comments){
	    if (comments[0] != undefined){
		res.send('<a>"https://testflight.apple.com/join/mbNLAcmw">Download</a>');
	    }
	    else{
		res.send('Token Error. Please Unregister and Register Again.');
	    }
	});
    });
	
    /**
     * @swagger
     * /beta/register:
     *   post:
     *     tags:
     *       - user
     *     description: Creates a new user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: user
     *         description: user to create
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/userCreate'
     *     responses:
     *       201:
     *         description: User created
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/userCreate'
     *       422:
     *         description: Username or Publickey already existing
     *         schema:
     *           $ref: '#/definitions/responsesFail/userCreate'
     */
    app.post(betaRegisterRoute, cors(), [ check('phoneType').exists(),
					  check('email').exists(),
					  check('email', 'Email Invalid').isEmail(),
					  check('email', 'Email already existing').custom(email => { return User.isEmailRegistered(email).then(value => { return !value; }); })], cors(),function(req, res) {
					      start = Date.now();
					      const errors = validationResult(req);
					      if (!errors.isEmpty()) {
						  log.write(Timer.display(start) + betaRegisterRoute + ' ' + JSON.stringify(errors.array(), null, 4));
						  return res.status(422).json({ errors: errors.array() });
					      }
					      log.write(betaRegisterRoute + ' ' + "Adding User for Beta: " + JSON.stringify(req.body, null, 4));
					      var token = Random.hex(64);
					      var user = new UserModel({ email: req.body.email, phoneType: req.body.phoneType, token: token, confirmed: false });
					      cache.set(req.body.email, JSON.stringify(user));
					      start = Date.now();
					      const job = AddUserQueue.createJob({email: req.body.email, phoneType: req.body.phoneType, token: token}).save().then((job) => {
						  job.on('succeeded', (result) => {
						      console.log("[" + Date.now() - start + `ms] Received result for job ${job.id}: ${job}`);
						      console.log(result);
						      res.json({
							  "message": result
						      });
						  }); 
					      });
					  });
    /**
     * @swagger
     * /beta/delete:
     *   post:
     *     tags:
     *       - user
     *     description: Creates a new user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: user
     *         description: user to create
     *         in: body
     *         required: true
     *         schema:
     *           $ref: '#/definitions/userCreate'
     *     responses:
     *       201:
     *         description: User created
     *         schema:
     *           $ref: '#/definitions/responsesSuccess/userCreate'
     *       422:
     *         description: Username or Publickey already existing
     *         schema:
     *           $ref: '#/definitions/responsesFail/userCreate'
     */
    app.post(betaDeleteRoute, cors(), [  check('email').exists(),
					 check('email').isEmail(),
					 check('email', "Email not registered").custom(email => { return User.isEmailRegistered(email).then(value => { return value;  }).catch(function(err){ return value; }) })], function(req, res) {
				     start = Date.now();
				     const errors = validationResult(req);
				     if (!errors.isEmpty()) {
					 log.write(Timer.display(start) + betaDeleteRoute + ' ' + JSON.stringify(errors.array(), null, 4));
					 return res.status(422).json({ errors: errors.array() });
				     }
					     log.write(betaDeleteRoute + ' ' + "Deleting User for Beta: " + JSON.stringify(req.body, null, 4));
					     cache.del(req.body.email, function(err, response){
						    if (response == 1) {
							console.log("[REDIS] Deleted Successfully! " + req.body.email);
						    } else{
							console.log("[REDIS] Cannot delete " + req.body.email);
						    }
					     });
				     const job = DelUserQueue.createJob({email: req.body.email}).save().then((job) => {
                                         job.on('succeeded', (result) => {
                                             console.log(`Received result for job ${job.id}: ${job}`);
                                             console.log(result);
                                             res.json({
						 "message": result
					     });
                                         });
                                     });
				 });

	/**
	 * @swagger
	 * /user/get/username:
	 *   post:
	 *     tags:
	 *       - user
	 *     description: Get username from public key
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - name: publicKey
	 *         description: publicKey of user you want to find.
	 *         in: body
	 *         required: true
	 *         schema:
	 *           $ref: '#/definitions/publicKey'
	 *     responses:
	 *       200:
	 *         description: username
	 *       422:
	 *         description: publicKey not found
	 */
    app.post(betaFeedbackRoute, cors(), [ check('phoneType').exists(),
				  check('description').exists(),
				  check('description').exists()], function(req, res) {
				      start = Date.now();
				      const errors = validationResult(req);
				      if (!errors.isEmpty()) {
					  log.write(Timer.display(start) + betaFeedbackRoute + ' ' + JSON.stringify(errors.array(), null, 4));
					  return res.status(422).json({ errors: errors.array() });
				      }
				      //console.log(util.inspect(req.body.files, false, null, true));
				      console.log(JSON.stringify(req.body.files));
				      var feedback = new FeedbackModel({ login: req.body.login, email: req.body.email, phoneType: req.body.phoneType, description: req.body.description });
				      cache.set(req.body.email, JSON.stringify(feedback));
                                      res.json({
					  message: "Feedback created"
                                      });
                                      feedback.save().then(() => {
					  console.log("Feedback save in DB");
                                      });
				  });    
}

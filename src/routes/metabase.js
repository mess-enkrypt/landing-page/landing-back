// Import Node Modules
const { check, validationResult } = require('express-validator');
var cors = require('cors');
const express = require('express');
const Queue = require('bee-queue');

// Import Misc Actions
const Timer = require('../actions/timer.js');
const log = require('../actions/discord.js');

const FeedbackModel = require('../models/Feedback.js');
const UserModel = require('../models/User.js');

const User = require('../actions/user.js');

var jwt = require("jsonwebtoken");

var start = new Date();

// Import Random function from random.js                                                                                      
const Random = require("../actions/random.js");

const util = require('util')
				     
const cache = require('../db/cache.js').client;
const subscriber = require('../db/cache.js').subscriber;


const AddUserQueue = new Queue("user/add",   {
    redis: {
	host: 'redis'
    },
    isWorker: false
});

const DelUserQueue = new Queue("user/delete",   {
    redis: {
	host: 'redis'
    },
    isWorker: false
});

/* ROUTES */
const getMetricsRoute = '/metabase/getMetrics';

module.exports = function(app){
    app.get(getMetricsRoute, cors(), function(req, res){
	start = Date.now();
	// you will need to install via 'npm install jsonwebtoken' or in your package.json
	var METABASE_SITE_URL = process.env.METABASE_SITE_URL || "https://metabase.messenkrypt.com";
	var METABASE_SECRET_KEY = process.env.METABASE_SECRET_KEY;
	var payload = {
	    resource: { question: 1 },
	    params: {},
	    exp: Math.round(Date.now() / 1000) + (10 * 60) // 10 minute expiration
	};
	var token = jwt.sign(payload, METABASE_SECRET_KEY);
	var iframeUrl = METABASE_SITE_URL + "/embed/question/" + token + "#theme=night&bordered=false&titled=false";
	log.write({timer: Timer.display(start), iframeUrl, referer: req.get('Referrer'), route: getMetricsRoute});
	res.json({totalUsers: iframeUrl});
    });
}

const express = require('express');
var cors = require('cors');
const { check } = require('express-validator');

// Import Misc Actions
const Timer = require('./actions/timer.js');
const log = require('./actions/discord.js');

const cache = require('./db/cache.js').client;
const subscriber = require('./db/cache.js').subscriber;

// Import Swagger
var swaggerUi = require('swagger-ui-express');
var swaggerJSDoc = require('swagger-jsdoc');

const port = process.env.PORT || 3000;
const app = express();

// Decode body request with json
app.use(express.json());

app.options('*', cors());

/*var corsOptions = {
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 200
};*/

/*app.use(function(req, res, next) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Origin, Accept, Content-Type, X-Requested-With, auth_token, X-CSRF-Token, Authorization');
    res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT, PATCH');
    res.set('Access-Control-Allow-Credentials', 'true');

    // intercept OPTIONS method
    if (req.method === 'OPTIONS') {
        return res.status(200).end();
    }

    next();
});
*/
app.all('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
 });

require("./routes/metabase.js")(app, cors);
require("./routes/beta.js")(app, cors);

var swaggerDefinition = {
    info: {
	title: 'MessEnkrypt Landing API',
	version: '1.0.0',
	description: 'Docs to use MessEnkrypt Landing API',
    },
    //host: 'localhost:3000',
    basePath: '/',
};

// options for the swagger docs
var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['index.js', 'routes/*.js'],// pass all in array

};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.get('/', (req, res) => res.send('Messenkrypt Landing API'));
app.get('/swagger.json', function(req, res) {   res.setHeader('Content-Type', 'application/json');   res.send(swaggerSpec); });
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.get('/health', function(req, res) {   res.setHeader('Content-Type', 'application/json');   res.send({"msg": "I'm alive"}); });


app.listen(port, () => console.log(`Messenkrypt Landing API started\nListening on port ${port} !`));



/**
 * @swagger
 * definition:
 *   userCreate:
 *     properties:
 *       username:
 *         type: string
 *       publicKey:
 *         type: string
 *   userDestroy:
 *     properties:
 *       username:
 *         type: string
 *       destroyPassphrase:
 *         type: string
 *   username:
 *     properties:
 *       username:
 *         type: string
 *   publickey:
 *     properties:
 *       publicKey:
 *         type: string
 *   destroyPassphrase:
 *     properties:
 *       destroyPassphrase:
 *         type: string
 *   newMsg:
 *     properties:
 *       from:
 *         type: string
 *       to:
 *         type: string
 *       msg:
 *         type: string
 *         description: THE ENCRYPTED MESSAGE
 *   toencrypt:
 *     properties:
 *       toencrypt:
 *         type: string
 *         description: String sent by mobile need to be encrypt by server for remote destroy
 *       username:
 *         type: string
 *   todecrypt:
 *     properties:
 *       toencrypt:
 *         type: string
 *         description: String sent by mobile need to be decrypt by server for unlock app
 *       username:
 *         type: string
 *   responsesSuccess:
 *     userCreate:
 *       properties:
 *         message:
 *           type: string
 *           description: Server response message
 *         username:
 *           type: string
 *           description: resuming username that is created
 *         publickey:
 *           type: string
 *           description: resuming the public that is associated to username
 *         destroyPassphrase:
 *           type: string
 *           description: the passphrase to activate remote destroy
 *     getPublicKey:
 *       properties:
 *         msg:
 *           type: string
 *           description: Server response message (intended for dev debug)
 *         username:
 *           type: string
 *           description: resuming username given
 *         publicKey:
 *           type: string
 *           description: the username's public key
 *     newMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: success message
 *     getMsg:
 *       properties:
 *         msg:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               _id:
 *                 type: string
 *                 description: ID Mongo DB (Proof Message are unique)
 *               from:
 *                 type: string
 *                 description: origin username
 *               to:
 *                 type: string
 *                 description: destination
 *               msg:
 *                 type: string
 *                 description: THE ENCRYPTED MESSAGE
 *               date:
 *                 type: string
 *                 description: date (format YYYY-MM-DDTHH:mm:ss.???)
 *   responsesFail:
 *     userCreate:
 *       properties:
 *         err:
 *           type: string
 *           description: telling if username or publickey is already existing
 *     getPublicKey:
 *       properties:
 *             err:
 *               type: string
 *               description: the error message
 *     newMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: error message
 *     getMsg:
 *       properties:
 *         msg:
 *           type: string
 *           description: server response (\"No New Msg or User not existing\")
 *     noUsernameFound:
 *       properties:
 *         err:
 *           type: string
 *           description: server response (\"Username not found\")
 */

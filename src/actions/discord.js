// Import the discord.js module
// const axios = require('axios')
const Discord = require('discord.js');

const DISCORD_LOG = process.env.DISCORD_LOG || false;

if (DISCORD_LOG != false && process.env.DISCORD_TOKEN === undefined || process.env.DISCORD_KEY === undefined)
    console.log("Douchebag, you need to give DISCORD_TOKEN && DISCORD_API if you want to write on discord");

// Create a new webhook
const hook = new Discord.WebhookClient(process.env.DISCORD_TOKEN, process.env.DISCORD_KEY);

module.exports = {
    write(value){
	if (DISCORD_LOG !== false && process.env.DISCORD_TOKEN != undefined && process.env.DISCORD_KEY != undefined)
	    hook.send(`\`\`\`json\n${  JSON.stringify(value)  }\`\`\``);
	console.log(value);

	if (process.env.LOGSTASH === "true"){
	    
	    const https = require('https')
	    
	    const options = {
		hostname: process.env.LOGSTASH_URL,
		port: process.env.LOGSTASH_PORT,
		path: process.env.LOGSTASH_PATH,
		method: 'POST',
		headers: {
		'Content-Type': 'application/json',
		    'Content-Length': JSON.stringify(value).length
		}
	    }
	    
	    const req = https.request(options, (res) => {
	    // console.log(`statusCode: ${res.statusCode}`)
		
		res.on('data', (d) => {
		    // process.stdout.write(d)
		})
	    })
	    
	req.on('error', (error) => {
	    console.error(error)
	})
	    
	    req.write(JSON.stringify(value))
	    req.end()
	
	}
    }
}
// Send a message using the webhook
// hook.send('I am now alive!');

// Import User Model
const UserModel = require('../models/User.js');

const cache = require('../db/cache.js').client;


module.exports = {
    isEmailRegistered: function (email){
	return new Promise( function(resolve, reject) {
            cache.exists(email, (err, result) => {
                if (result === 1){
		    resolve(true);
		}
                else{
                    UserModel.find({email: email}, function(error, comments){
                        if (comments[0] !== undefined){
                            cache.set(email, JSON.stringify(comments[0]));
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    });
                }
            });
        });
    },
    delete: function (email){
	return new Promise( function(resolve, reject) {
	    UserModel.find({email: email}, function(error, comments){
		if (comments[0] !== undefined)
		{
		    UserModel.deleteMany({email: email}, function(error, comments){
			resolve("User deleted");
		    });
		}
		else
		    reject(false);
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    }
};

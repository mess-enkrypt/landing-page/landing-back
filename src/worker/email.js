const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: 587,
    secure: false, // upgrade later with STARTTLS
    auth: {
	user: process.env.MAIL_USER,
	pass: process.env.MAIL_PASSWORD
    },
    tls: {
	// do not fail on invalid certs
	rejectUnauthorized: false
    }
});

// verify connection configuration
transporter.verify(function(error, success) {
  if (error) {
    console.log(error);
  } else {
    console.log("Connected to Mail Server");
  }
});


module.exports = transporter;

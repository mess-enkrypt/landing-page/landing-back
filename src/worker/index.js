const Queue = require('bee-queue');

const FeedbackModel = require('../models/Feedback.js');
const UserModel = require('../models/User.js');

const User = require('../actions/user.js');

const transporter = require('./email.js');

var start = new Date();

const AddUserQueue = new Queue("user/add",   {
    redis: {
	host: 'redis'
    },
    isWorker: true
});

const DelUserQueue = new Queue("user/delete",   {
    redis: {
	host: 'redis'
    },
    isWorker: true
});


AddUserQueue.process(function(job) {
    return new Promise(function(resolve, reject) {
	console.log(`Processing job Add User ${job.id}`);
	var user = new UserModel({ email: job.data.email, phoneType: job.data.phoneType, token: job.data.token, confirmed: false });
	user.save().then(() => {
            console.log("user save in DB");
	    if (job.data.phoneType === "ios"){
		var mailOptions = {
		    from: "beta@messenkrypt.com",
		    to: job.data.email,
		    subject: "Beta Link iOS Download",
		    text: "Welcome on MessEnkrypt,\nHere a link to download app: https://messenkrypt.com/download/${job.data.token}",
		    html: '<h1>Welcome on MessEnkrypt</h1><br /><p>Here a link to download app: </p><a href="https://api.messenkrypt.xyz/download/' + job.data.token + '"><button>Go to Download Page</button></a>'
		};
		console.log("Sending Email to " + job.data.email);
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
			return console.log(error);
		    }
		    console.log('Message status: ' + info.response);
		    resolve(user.email  + " added");
		    transporter.close();
		});
	    }
	    else{
		resolve(user.email  + " added");
	    }
	}).catch(function(err){ return new Error(err); });
	//throw new Error("Email not Sent");
    });
});



DelUserQueue.process(function(job){
    return new Promise(function(resolve, reject) {
	start = Date.now();
	console.log(`Processing job Del User ${job.id}`);
	User.isEmailRegistered(job.data.email).then(function(value){
	    if (value){
		User.delete(job.data.email).then(value => {
		    console.log(value);
		    resolve(value);
		}).catch(function(err){
		    reject(false);
		});
	    }
	    else
		reject("User not registered");
	}).catch(function(err){ return err; })
    });
});
//throw new Error("Email not Sent"

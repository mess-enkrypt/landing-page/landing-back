var db = require('../db/db.js');
var Feedback = db.model('Feedback', {
    login: { type: String, required: false },
    email: { type: db.SchemaTypes.Email, required: false },
    phoneType: { type: [String], required: true },
    description: { type: String, required: true },
    files: { type: [String], required: false  },
    created: { type: Date, default: Date.now, required: true},
    updated: { type: Date, default: Date.now, required: true },
})
module.exports = Feedback

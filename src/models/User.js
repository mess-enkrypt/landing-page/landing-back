var db = require('../db/db.js');
var User = db.model('User', {
    email:     { type: db.SchemaTypes.Email, unique: true, lowercase: true, required: true },
    phoneType: { type: [String], required: true },
    token: { type: String, required: false },
    confirmed: { type: Boolean, required: true },
    created: { type: Date, default: Date.now, required: true},
    updated: { type: Date, default: Date.now, required: true },
})
module.exports = User

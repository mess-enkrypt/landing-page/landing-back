# Mess-Enkrypt API

Requirements:
   - docker
   - docker-compose
   - make

Don't forget to init, if not, will not works

Init Project:
	```make init```

Launch attached in term:
	```make dev```

Launch:
	```make start```

Stop:
	```make stop```

Delete containers:
	```make clean```

Install docker:
	```make install_dc```

Install docker-compose:
	```m̀ake install_dcc```

Launch Test:
	```make test```
	
Remove Test:
	```make test_clean```

Re Test:
	```make test_re```
	
Logs Test:
	```make test_logs```
	
Launch LoadTest:
```make loadtest```
	
Remove LoadTest:
	```make loadtest_clean```

Re LoadTest:
	```make loadtest_re```
	
Logs Test:
	```make loadtest_logs```

```
project
|   actions
    |   crypt.js (functions to encrypt and decrypt string send by user with his serverEncryptionKey and keep this key in DB)
    |   discord.js (functions to log in console.log and discord channel)
    |   mailbox.js (functions to use Mailbox (add msg, getUserMsg, etc))
    |   random.js  (function to generate random to encrypt in crypt.js)
    |   timer.js (functions to keep track of time used by actions in API)
    |   user.js  (functions to use User (add user, getUsername, etc))
|   config
    |   dev.config.js (config JS for PM2 in local dev mode to watch files)
|   db (Contains DB files)
    |   db.js (Setup DB)
|   docker-compose.yml (Manage docker containers in local dev)
|   Dockerfile (Use to build docker image in local dev)
|   DockerfileCI (Use to build docker image in CI)
|   index.js (routes and controllers)
|   Makefile (`make` rules to launch project)
|   models (Models for Database)
    |   Msg.js (Models for Msg / Mailbox)
    |   User.js (Models for User)
|   package.json (dependencies and npm rules)
|   README.md (documentation)
|   test.js (some tests on actions)
```

When http requests triggers a route, it triggers it in `index.js` and then the controllers call the appropriate `actions/` like `User.create()`.

HTTP Status:
 - Good, server return `200`.
 - Wrong, server return `422`.

- Watcher:
	- We are using `PM2` to watch files in local dev and relaunch node while changes.
	- The config file is in `config/` directory.
	
- Logger:
	- We are using `discordjs` to use Discord webhooks to write in Discord channel, same log from `console.log()`.
	- Please use `log.write()` instead of `console.log()` for general logging.
	
- Timer:
	- To use timer, import `actions/timer.js` and then use `Timer.display(start)` that return `[time in ms]`.
	- `start` must be a `new Date()` or `Date.now()` to use start time.

- Promise:
	- We don't use `callback()` but `Promise`.
	- That's why a lot of function have `test().then(function(value){ console.log(value); })`.
	- To end a `Promise`:
		- if successful use `resolve(returnValue)`
		- If not `reject(err)` and the error will be catch by the `.catch()` so we can make different json msg
